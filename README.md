# README #

Dockerized-wildfly shows how to use docker to run your application.

### Whats in? ###

* Dockerfile - defines our [http://wildfly.org/](wildfly) image 
* build_container.sh - builds and tags our wildfly container image
* checkout_service.sh - checks out a jaxrs service to make a complete example
* run_container.sh - runs the docker image, mounts a local filesystem folder and publishes application and management ports

### How to use it? ###

1. checkout service
2. build container image
3. run image and build service
4. do development, rebuild the service and watch wildflys logfile

Have fun
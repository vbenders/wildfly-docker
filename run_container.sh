#!/bin/sh

HOST_DIR=$(pwd)/jaxrs2-fileupload/target
CONTAINER_DIR=/opt/jboss/wildfly/standalone/deployments 
TAG=vbenders/wildfly-admin

docker run -it -p 8080:8080 -p 9990:9990 -v $HOST_DIR:$CONTAINER_DIR $TAG

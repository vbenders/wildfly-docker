#
# base our work on the latest version of wildfly
#
FROM jboss/wildfly
MAINTAINER vbenders@gmail.com
#
# create an admin user
#
RUN /opt/jboss/wildfly/bin/add-user.sh --silent --user admin --password admin

#
# run server w app and mgmt ports available
#
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]

#eof

